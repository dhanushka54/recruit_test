import 'package:flutter/material.dart';

class PageNavigator {
  static void goToLogin(BuildContext context) {
    Navigator.pushNamed(context, "/Login");
  }
}
