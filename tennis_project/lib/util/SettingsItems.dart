class SettingsItems {
  static const String UserProfile = 'UserProfile';
  // static const String Notification = 'Notification';
  // static const String Sound = 'Sound';
  // static const String Language = 'Language';
  static const String Version = 'Version';
  static const String ContactUs = 'Contact Us';
  static const String Help = 'Help';
  static const String Logout = 'Log out';

  static const List<String> choices = <String>[
    UserProfile,
    // Notification,
    // Sound,
    // Language,
    Version,
    ContactUs,
    Help,
    Logout,

  ];
}
