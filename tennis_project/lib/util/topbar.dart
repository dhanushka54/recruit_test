import 'package:firebase_admob/firebase_admob.dart';
import 'package:tenizo/layout/contact_us.dart';
import 'package:tenizo/layout/help.dart';
import 'package:tenizo/layout/version.dart';
import 'package:tenizo/styles/app_style.dart';
import 'package:tenizo/util/SettingsItems.dart';
import 'package:flutter/material.dart';
import '../layout/edit_user_details.dart';

class TopBar extends StatefulWidget implements PreferredSizeWidget {
  final String headerTitle;
  final bool showBackBtn;
  final bool showShareBtn;

  final Function onBackClicked;
  final Function logoutButton;
  final Function onSelected;
  final Function onPageChanged;
  final BuildContext ctx;
  final Function screenShot;
  final Function initAd;
  final Function showAd;

  @override
  _TopBarState createState() => _TopBarState();
  TopBar({
    Key key,
    this.headerTitle,
    this.showBackBtn,
    this.showShareBtn,
    this.onBackClicked,
    this.logoutButton,
    this.onSelected,
    this.onPageChanged,
    this.ctx, 
    this.screenShot,
    this.initAd,
    this.showAd,
  }) : super(key: key);

  @override
  Size get preferredSize => new Size.fromHeight(kToolbarHeight);
}

class _TopBarState extends State<TopBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: MediaQuery(
        data: MediaQueryData(),
        child: Text(
          widget.headerTitle,
          style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700),
        ),
      ),
      leading: widget.showBackBtn
          ? IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () {
                widget.onBackClicked(true, false);
              },
            )
          : null,
      automaticallyImplyLeading: false,
      actions: <Widget>[
        widget.showShareBtn
            ? IconButton(
                icon: Icon(Icons.share),
                onPressed: () {
                  widget.showAd(1);
                },
              )
            : Container(),
        PopupMenuButton<String>(
          icon: Icon(
            Icons.more_vert,
            color: Colors.white,
            size: 28,
          ),
          onSelected: choiceAction,
          itemBuilder: (BuildContext context) {
            return SettingsItems.choices.map((String choice) {
              return PopupMenuItem<String>(
                value: choice,
                child: MediaQuery(
                    data: MediaQueryData(),
                    child: Container(width: 100, child: Text(choice))),
              );
            }).toList();
          },
        ),
      ],
      centerTitle: true,
      backgroundColor: AppStyle.color_AppBar,
    );
  }

  void choiceAction(String choice) {
//Login button function-----------------------------
    if (choice == "Log out") {
      widget.logoutButton();
    } else if (choice == "UserProfile") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => EditUser()),
      );
    } else if (choice == "Help") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => HelpPage()),
      );
    } else if (choice == "Contact Us") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ContactUs()),
      );
    }
    else if (choice == "Version") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Version()),
      );
    }
  }
}
