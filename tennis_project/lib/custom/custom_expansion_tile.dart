import 'package:tenizo/layout/stadium_registration.dart';
import 'package:tenizo/styles/app_style.dart';
import 'package:flutter/material.dart';

class CustomExpansionTile extends StatefulWidget {
  String title;
  List<Widget> listData;
  Function onButtonPressed;

  CustomExpansionTile({this.title, this.listData, this.onButtonPressed});
  @override
  State createState() => CustomExpansionTileState();
}

class CustomExpansionTileState extends State<CustomExpansionTile> {
  bool isExpanded = false;

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQueryData(),
      child: Column(
        children: <Widget>[
          Container(
            child: ExpansionTile(
              title: Text(
                widget.title,
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
              trailing: Icon(
                isExpanded
                    ? Icons.keyboard_arrow_down
                    : Icons.keyboard_arrow_right,
                size: 36.0,
                color: AppStyle.color_Head,
              ),
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Column(
                      children: widget.listData,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        IconButton(
                          icon: Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(60.0),
                                color: AppStyle.color_Head),
                            child: Icon(
                              Icons.add,
                              size: 40,
                              color: Colors.white,
                            ),
                          ),
                          // splashColor: Colors.grey,
                          iconSize: 50,
                          onPressed: widget.onButtonPressed,
                        ),
                      ],
                    ),
                  ],
                ),
              ],
              onExpansionChanged: (bool expanding) =>
                  setState(() => this.isExpanded = expanding),
            ),
          ),
        ],
      ),
    );
  }
}
