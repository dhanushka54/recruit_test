import 'dart:math';
class Score {


  static dateFormatter(date) {
    var selectedyear = date.year;
    var selectedmonth = 'Jan';
    var selectedDate = date.day;
    switch (date.month) {
      case 1:
        selectedmonth = 'Jan';
        break;
      case 2:
        selectedmonth = 'Feb';
        break;
      case 3:
        selectedmonth = 'Mar';
        break;
      case 4:
        selectedmonth = 'Apr';
        break;
      case 5:
        selectedmonth = 'May';
        break;
      case 6:
        selectedmonth = 'Jun';
        break;
      case 7:
        selectedmonth = 'Jul';
        break;
      case 8:
        selectedmonth = 'Aug';
        break;
      case 9:
        selectedmonth = 'Sept';
        break;
      case 10:
        selectedmonth = 'Oct';
        break;
      case 11:
        selectedmonth = 'Nov';
        break;
      case 12:
        selectedmonth = 'Dec';
        break;
      default:
    }
    return '$selectedmonth $selectedDate , $selectedyear';
  }

    static timeFormatter(date) {
    var hr = date.hour;
    if (hr < 10) {
      hr = '0$hr';
    }
    var min = date.minute;
    if (min < 10) {
      min = '0$min';
    }
    var sec = date.second;
    if (sec < 10) {
      sec = '0$sec';
    }
    return '$hr : $min : $sec';
  }


  static dbtimeFormatter(date){
        var hr = date.hour;
    if (hr < 10) {
      hr = '0$hr';
    }
    var min = date.minute;
    if (min < 10) {
      min = '0$min';
    }
    var sec = date.second;
    if (sec < 10) {
      sec = '0$sec';
    }
    return '$hr:$min:$sec';
  }

  static degToRad(double deg) => deg * (pi / 180.0);

  static getx(double val) {
    return -1 * cos(degToRad(val));
  }

  static gety(double val) {
    return -1 * sin(degToRad(val));
  }

}


