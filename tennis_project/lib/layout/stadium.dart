import 'package:flutter/material.dart';

class Stadium extends StatelessWidget implements PreferredSizeWidget {
  final String userName;
  const Stadium({
    Key key,
    this.userName
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
          child: MediaQuery(
        data: MediaQueryData(),
        child: Container(
          alignment: AlignmentDirectional.center,
          color: Color(0xFFE5F9E0),
          child: Text(
            "Stadium",
            style: TextStyle(fontSize: 40.0, color: Colors.black),
          ),
        ),
      ), onWillPop:  () async => Future.value(
              false),
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(kToolbarHeight);
}
