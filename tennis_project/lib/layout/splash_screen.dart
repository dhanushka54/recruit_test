import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tenizo/util/app_navigator.dart';
import 'package:tenizo/util/app_const.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 1), () => PageNavigator.goToLogin(context));
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQueryData(),
      child: Scaffold(
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: new DecorationImage(
                  image: AssetImage('images/splash_background.png'),
                  repeat: ImageRepeat.repeat,
                ),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: Container(
                            child:SvgPicture.asset('images/splashLogo.svg'),
                          ),
                        ),
            
                        Padding(
                          padding: EdgeInsets.only(top: 130.0), //10
                        ),
                        Text(
                          AppTerms.projectName,
                          style: TextStyle(
                              color: AppColors.splash_font,
                              fontWeight: FontWeight.w800,
                              fontSize: 65.0), //36
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 5.0),
                        ),
                        Text(
                          AppTerms.projectDescription,
                          style: TextStyle(
                              color: AppColors.black,
                              fontWeight: FontWeight.w800,
                              fontSize: 20.0), //24
                        )
                      ],
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
